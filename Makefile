all: bin/edit 
all: static/codemirror/lib/codemirror.js

bin/edit: FORCE
	GOPATH=$$PWD go install edit

static/codemirror/lib/codemirror.js: static/codemirror/rollup.config.js $(shell find static/codemirror/src -name '*.js')
	cd static/codemirror && npm install && npm run build && rm -rf node_modules

.PHONY: FORCE
