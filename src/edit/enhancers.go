package main

import (
	"io"
	"io/ioutil"
	"os"
	"mime/multipart"
	"strings"
	"path"
)

type enhancer struct {
	pattern []string
	head    string
	tail    string
}

func loadEnhancers(filename string) []enhancer {
	file, err := os.Open(filename)
	errcheck(err)

	reader := multipart.NewReader(file, "boundary")
	
	var s uint8 = 0
	var e enhancer
	var ret []enhancer
	for {
		part, err := reader.NextPart()
		if err == io.EOF {
			return ret
		}
		errcheck(err)
		
		k := part.Header.Get("X-Thing")
		v, err := ioutil.ReadAll(part)
		errcheck(err)

		switch k {
		case "Pattern":
			patterns := strings.Split(string(v), ";")
			for i := range patterns {
				patterns[i] = strings.TrimSpace(patterns[i])
			}
			s |= 1<<0
			e.pattern = patterns
		case "Head":
			s |= 1<<1
			e.head = string(v)
		case "Tail":
			s |= 1<<2
			e.tail = string(v)
		default:
			panic("unknown X-Thing: "+k)
		}
		if s == 1<<0 | 1<<1 | 1<<2 {
			ret = append(ret, e)
			s = 0
		}
	}
}

var enhancers = loadEnhancers("enhancers.txt")

func getEnhancer(ctype string) (head, tail string) {
	for _, enhancer := range enhancers {
		for _, pattern := range enhancer.pattern {
			matched, err := path.Match(pattern, ctype)
			errcheck(err)
			if matched {
				return enhancer.head, enhancer.tail
			}
		}
	}
	return "", ""
}
