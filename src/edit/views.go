package main

import (
	"fmt"
	"bytes"
	"io"
	"path"
	"strings"
	"text/template"
)

func newTemplate(filenames ...string) *template.Template {
	return template.Must(template.New(path.Base(filenames[0])).
		Funcs(template.FuncMap{
			"istext": istext,
		}).
		ParseFiles(filenames...))
}

var (
	tmplPage     = newTemplate("got/page.html.got")
	tmplViewTree = newTemplate("got/view_tree.got")
	tmplViewBlob = newTemplate("got/view_blob.got")
	tmplModified = newTemplate("got/modified.got")
	tmplDeleted  = newTemplate("got/deleted.got")
)

func renderPage(w io.Writer, title, head, body, tail string) error {
	return tmplPage.Execute(w, map[string]string{
		"title": title,
		"head":  head,
		"body":  body,
		"tail":  tail,
	})
}

func renderViewTree(w io.Writer, upath string, tree GitTree) error {
	// Pre-processing
	files := make(GitTree)
	for name, file := range tree {
		if !strings.HasPrefix(name, upath) {
			continue
		}
		name = strings.TrimPrefix(name, upath)
		if len(name) == 0 || strings.Contains(name, "/") {
			continue
		}
		files[name] = file

	}
	// Component Render
	var body bytes.Buffer
	err := tmplViewTree.Execute(&body, map[string]interface{}{
		"path":  upath,
		"files": files,
	})
	if err != nil {
		return err
	}
	head, tail := getEnhancer("inode/directory")
	// Page render
	return renderPage(w, upath, head, body.String(), tail)
}

func renderViewBlob(w io.Writer, upath string, file *GitFile) error {
	var err error
	var content []byte
	// Pre-processing
	if file != nil {
		content, err = file.Cat()
		if err != nil {
			return err
		}
	}
	ctype := getctype(upath, content)
	// Component render
	var body bytes.Buffer
	err = tmplViewBlob.Execute(&body, map[string]interface{}{
		"path":    upath,
		"ctype":   ctype,
		"content": string(content),
		"exists":  file != nil,
	})
	if err != nil {
		return err
	}
	
	head, tail := getEnhancer(ctype)
	head = fmt.Sprintf("<script>ctype = \"%s\"</script>\n%s", template.JSEscapeString(ctype), head);

	// Page render
	return renderPage(w, upath, head, body.String(), tail)
}

func renderModified(w io.Writer, upath string) error {
	// Component render
	var body bytes.Buffer
	err := tmplModified.Execute(&body, map[string]string{
		"path": upath,
	})
	if err != nil {
		return err
	}
	// Page render
	return renderPage(w, upath, "", body.String(), "")
}

func renderDeleted(w io.Writer, upath string) error {
	// Component render
	var body bytes.Buffer
	err := tmplDeleted.Execute(&body, map[string]string{
		"path": upath,
	})
	if err != nil {
		return err
	}
	// Page render
	return renderPage(w, upath, "", body.String(), "")
}
