package main

import (
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

func SeekerLen(s io.Seeker) int64 {
	cur, _ := s.Seek(0, io.SeekCurrent)
	end, _ := s.Seek(0, io.SeekEnd)
	s.Seek(cur, io.SeekStart)
	return end
}

func PostHack(req *http.Request) {
	if req.Method == http.MethodPost {
		// _method
		if method := req.PostFormValue("_method"); method != "" {
			req.Method = method
			req.PostForm.Del("_method")
		}
		// _message
		if msg := req.PostFormValue("_message"); msg != "" {
			req.Header.Set("X-Commit-Message", msg)
			req.PostForm.Del("_message")
		}
		// _body
		if file, header, err := req.FormFile("_body"); err == nil {
			req.Body = file
			for k, v := range header.Header {
				req.Header[k] = v
			}
			if header.Header.Get("Content-Length") == "" {
				req.Header.Del("Content-Length")
			}
			req.ContentLength = SeekerLen(file)

			// Clear caches
			req.Form = nil
			req.PostForm = nil
			req.MultipartForm = nil
		} else if bodies := req.PostForm["_body"]; len(bodies) > 0 {
			body := bodies[0]
			if req.PostFormValue("_body_decrlf") != "" {
				body = strings.Replace(body, "\r\n", "\n", -1)
			}
			req.Body = ioutil.NopCloser(strings.NewReader(body))
			if req.Header.Get("Content-Length") != "" {
				req.Header.Set("Content-Length", strconv.Itoa(len(body)))
			}
			req.ContentLength = int64(len(body))

			// Clear caches
			req.Form = nil
			req.PostForm = nil
			req.MultipartForm = nil
		}
	}
}
