package main

import (
	"mime"
	"net/http"
	"os/exec"
	"path"
	"strings"
)

type exitError exec.ExitError

func (e *exitError) Error() string {
	ret := e.ProcessState.String()
	if len(e.Stderr) > 0 {
		ret += "\n" + string(e.Stderr)
	}
	return ret
}

func errcheck(err error) {
	if err != nil {
		if ee, ok := err.(*exec.ExitError); ok {
			ee2 := exitError(*ee)
			err = &ee2
		}
		panic(err)
	}
}

func getctype(name string, content []byte) string {
	ctype := mime.TypeByExtension(path.Ext(name))
	if ctype == "" {
		ctype = http.DetectContentType(content)
	}

	i := strings.Index(ctype, ";")
	if i == -1 {
		i = len(ctype)
	}
	ctype = strings.TrimSpace(strings.ToLower(ctype[0:i]))

	return ctype
}

func istext(ctype string) bool {
	return strings.HasPrefix(ctype, "text/") || strings.HasSuffix(ctype, "+xml")
}
