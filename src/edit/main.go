package main

import (
	"net/http"
	"os"
	"util"
)

func ServeIndex(out http.ResponseWriter, in *http.Request) {
	if in.URL.Path != "/" {
		http.NotFound(out, in)
		return
	}
	http.Redirect(out, in, "/files/src/", http.StatusMovedPermanently)
}

var WWW = GitDir("/srv/http/edit.team4272.com/www.git")

func main() {
	socket, err := util.StreamListener(os.Args[1], os.Args[2])
	errcheck(err)
	http.Handle("/", util.SaneHTTPHandler{http.HandlerFunc(ServeIndex)})
	http.Handle("/static/", util.SaneHTTPHandler{http.StripPrefix("/static", http.FileServer(http.Dir("static")))})
	http.Handle("/files/", util.SaneHTTPHandler{http.StripPrefix("/files", http.HandlerFunc(ServeGit))})
	errcheck(http.Serve(socket, nil))
}
