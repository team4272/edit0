--boundary
X-Thing: Pattern

inode/directory
--boundary
X-Thing: Head

--boundary
X-Thing: Tail

<script>
(function() {
	// todo: input to create new page, upload button
})();
</script>
--boundary
X-Thing: Pattern

text/markdown
--boundary
X-Thing: Head

<link rel="stylesheet" href="/static/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/static/simplemde/dist/simplemde.min.css">
<script src="/static/simplemde/dist/simplemde.min.js"></script>
--boundary
X-Thing: Tail

<script>
(function() {
	var textarea = document.getElementsByTagName('textarea')[0];
	var form = textarea.form;
	var container = document.createElement('div');
	form.insertBefore(container, textarea);
	container.appendChild(textarea);

	var tip = document.createElement('aside');
	tip.innerHTML =
		'<p>Tip: To set the page title (what appears in the tab '+
		'name/window bar), but not have it appear as a header on the page, put '+
		'this at the top:</p>\n'+
		'<pre>---\ntitle: "Your Title Here"\n---\n</pre>\n'+

		'<p>To set the page title, and have it appear as a header on the page, '+
		'put this at the top:<p>\n'+
		'<pre>Your Title Here\n===============\n</pre>\n';

	container.appendChild(tip);

	// workaround from https://github.com/codemirror/CodeMirror/issues/2143#issuecomment-140100969
	var updateSectionHeaderStyles = function(cm, change) {
		var lines = cm.lineCount();
		for (var i = Math.max(0, change.from.line-1); i <= Math.min(change.to.line+1, lines-1); i++) {
			var line = cm.getLineHandle(i);
			cm.removeLineClass(line, 'text', 'cm-header');
			cm.removeLineClass(line, 'text', 'cm-header-1');
			cm.removeLineClass(line, 'text', 'cm-header-2');
			var lineTokens = cm.getLineTokens(i);
			var tok = lineTokens[0];
			if (!tok || !tok.type || tok.type.indexOf('header') === -1) {
				// first token could be some spaces, try 2nd
				tok = lineTokens[1];
			}
			if (tok && tok.type && tok.type.indexOf('header') !== -1
			    && tok.string !== '#') { // not ATX header style, which starts with #
				var classes = tok.type.
				    split(' ').
				    filter(function(cls) { return cls.indexOf('header') === 0; }).
				    map(function (cls) { return 'cm-' + cls; }).
				    join(' ');
				var prev_line = cm.getLineHandle(i-1);
				cm.addLineClass(prev_line, 'text', classes);
			}
		}
	};

	var simplemde = new SimpleMDE({
		autoDownloadFontAwesome: false,
		element: textarea,
		promptURLs: true,
		showIcons: ['code', 'table'],
	});
	var cm = simplemde.codemirror;
	cm.on("change", function(cm, change) {
		cm.save(); // this is SimpleMDE forceSync
		updateSectionHeaderStyles(cm, change);
	});
	updateSectionHeaderStyles(cm, {from: {line: 0}, to: {line: cm.lineCount()}});
})();
</script>
--boundary
X-Thing: Pattern

text/*; */*+xml; application/x-ruby
--boundary
X-Thing: Head

<link rel="stylesheet" href="/static/codemirror/lib/codemirror.css">
<script src="/static/codemirror/lib/codemirror.js"></script>

<script src="/static/codemirror/mode/xml/xml.js"></script>
<script src="/static/codemirror/mode/javascript/javascript.js"></script>
<script src="/static/codemirror/mode/css/css.js"></script>
<script src="/static/codemirror/mode/sass/sass.js"></script>
<script src="/static/codemirror/mode/ruby/ruby.js"></script>
<script src="/static/codemirror/mode/htmlmixed/htmlmixed.js"></script>

<script src="/static/codemirror/addon/edit/matchbrackets.js"></script>
<script src="/static/codemirror/addon/edit/trailingspace.js"></script>
<script src="/static/codemirror/addon/display/rulers.js"></script>

<script src="/static/codemirror/addon/fold/foldcode.js"></script>
<script src="/static/codemirror/addon/fold/foldgutter.js"></script>
<link rel="stylesheet" href="/static/codemirror/addon/fold/foldgutter.css">
<script src="/static/codemirror/addon/fold/brace-fold.js"></script>
<script src="/static/codemirror/addon/fold/xml-fold.js"></script>
<script src="/static/codemirror/addon/fold/markdown-fold.js"></script>
<script src="/static/codemirror/addon/fold/comment-fold.js"></script>
--boundary
X-Thing: Tail

<script>
(function() {
	var textarea = document.getElementsByTagName('textarea')[0];
	var form = textarea.form;
	var container = document.createElement('div');
	form.insertBefore(container, textarea);
	container.appendChild(textarea);
	var cm = CodeMirror.fromTextArea(textarea, {
		mode: ctype,
		lineNumbers: true,
		matchBrackets: true,
		foldGutter: true,
		rulers: [{column: 72, color: '#DDDDDD', lineStyle: "dashed"}, {column: 80, color: '#F4DDDD', lineStyle: "dashed"}],
		showTrailingSpace: true,
		gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
	});
	cm.on("change", function() {
		cm.save();
	});
})();
</script>
--boundary--
